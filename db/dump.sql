CREATE DATABASE  IF NOT EXISTS `kao` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `kao`;
-- MySQL dump 10.13  Distrib 5.7.12, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: kao
-- ------------------------------------------------------
-- Server version	5.7.16-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `Kaomojis`
--

DROP TABLE IF EXISTS `Kaomojis`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kaomojis` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` longblob NOT NULL,
  `name` varchar(255) CHARACTER SET latin1 DEFAULT NULL,
  `description` varchar(2000) CHARACTER SET latin1 DEFAULT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `views` int(11) unsigned NOT NULL DEFAULT '0',
  `likes` int(11) unsigned NOT NULL DEFAULT '0',
  `dislikes` int(11) unsigned NOT NULL DEFAULT '0',
  `approved` int(1) NOT NULL DEFAULT '0',
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `FK_Kaomojis_Users_idx` (`user_id`),
  CONSTRAINT `FK_Kaomojis_Users` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kaomojis`
--

LOCK TABLES `Kaomojis` WRITE;
/*!40000 ALTER TABLE `Kaomojis` DISABLE KEYS */;
INSERT INTO `Kaomojis` VALUES (2,'\\u1555\\u0028\\u0020\\u141b\\u0020\\u0029\\u1557\\u0020','Happy Gary','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2016-12-13 05:05:23','2016-12-15 18:30:31',151,10,5,1,2),(20,'(;_;)','cool','beans','2016-12-15 10:37:25','2016-12-15 18:31:48',31,0,0,1,2),(36,'%C2%AF%5C_(%E3%83%84)_%2F%C2%AF','Shrug','I don\'t know...','2016-12-15 14:06:19','2016-12-15 14:11:29',3,0,0,1,3),(37,'%20%E2%94%90_%E3%8B%A1_%E2%94%8C','Shrug 2','I don\'t know...','2016-12-15 14:07:03','2016-12-15 14:11:34',3,0,0,1,3),(38,'%CA%95%20%E2%80%A2%E1%B4%A5%E2%80%A2%CA%94','Bear','','2016-12-15 14:09:59','2016-12-15 18:33:45',7,0,0,1,3),(39,'%20%CA%95%E3%81%A3%CB%98%DA%A1%CB%98%CF%82%CA%94','mm','tasty','2016-12-15 14:10:34','2016-12-15 18:34:14',6,0,0,0,3),(40,'(%20%CD%A1%C2%B0%20%CD%9C%CA%96%20%CD%A1%C2%B0)%20','lenny','hmm','2016-12-15 14:11:15','2016-12-15 14:11:16',1,0,0,0,3),(41,'%CA%95%E3%80%80%C2%B7%E1%B4%A5%CA%94','Bear','','2016-12-15 14:12:00','2016-12-15 18:34:04',3,0,0,0,3),(42,'%E1%95%A6(%20%E1%90%9B%20)%E1%95%A1','Happy Gary 2','','2016-12-15 14:12:46','2016-12-15 14:13:25',3,0,0,1,3),(43,'%D9%A9(%20%E1%90%9B%20)%D9%88','Happy Gary 3','','2016-12-15 14:13:19','2016-12-15 14:13:21',1,0,0,1,3),(44,'%E1%95%A6(%CA%98%E2%97%A1%CA%98)%E1%95%A4','klsja','fjasklfasj','2016-12-15 18:32:21','2016-12-15 18:33:17',3,0,0,1,2);
/*!40000 ALTER TABLE `Kaomojis` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Kaomojis_Lists`
--

DROP TABLE IF EXISTS `Kaomojis_Lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kaomojis_Lists` (
  `list_id` int(11) unsigned NOT NULL,
  `kaomoji_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`list_id`,`kaomoji_id`),
  KEY `FK_Kaomojis_Lists_Kaomojis_idx` (`kaomoji_id`),
  CONSTRAINT `FK_Kaomojis_Lists_Kaomojis` FOREIGN KEY (`kaomoji_id`) REFERENCES `Kaomojis` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Kaomojis_Lists_Lists` FOREIGN KEY (`list_id`) REFERENCES `Lists` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kaomojis_Lists`
--

LOCK TABLES `Kaomojis_Lists` WRITE;
/*!40000 ALTER TABLE `Kaomojis_Lists` DISABLE KEYS */;
INSERT INTO `Kaomojis_Lists` VALUES (1,2);
/*!40000 ALTER TABLE `Kaomojis_Lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Kaomojis_Tags`
--

DROP TABLE IF EXISTS `Kaomojis_Tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Kaomojis_Tags` (
  `tag_id` int(11) unsigned NOT NULL,
  `kaomoji_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`tag_id`,`kaomoji_id`),
  KEY `FK_Kaomojis_Tags_Kaomojis_idx` (`kaomoji_id`),
  CONSTRAINT `FK_Kaomojis_Tags_Kaomojis` FOREIGN KEY (`kaomoji_id`) REFERENCES `Kaomojis` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `FK_Kaomojis_Tags_Tags` FOREIGN KEY (`tag_id`) REFERENCES `Tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Kaomojis_Tags`
--

LOCK TABLES `Kaomojis_Tags` WRITE;
/*!40000 ALTER TABLE `Kaomojis_Tags` DISABLE KEYS */;
INSERT INTO `Kaomojis_Tags` VALUES (1,2),(2,2),(12,20),(13,36),(15,36),(13,37),(15,37),(20,38),(1,39),(21,39),(22,39),(23,40),(24,40),(20,41),(21,41),(1,42),(2,42),(1,43),(2,43),(19,43),(25,44);
/*!40000 ALTER TABLE `Kaomojis_Tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Lists`
--

DROP TABLE IF EXISTS `Lists`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Lists` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modified_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  KEY `FK_Lists_Users_idx` (`user_id`),
  CONSTRAINT `FK_Lists_Users` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Lists`
--

LOCK TABLES `Lists` WRITE;
/*!40000 ALTER TABLE `Lists` DISABLE KEYS */;
INSERT INTO `Lists` VALUES (1,'likes','2016-12-13 05:06:53','2016-12-13 05:06:53',2);
/*!40000 ALTER TABLE `Lists` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Sessions`
--

DROP TABLE IF EXISTS `Sessions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sessions` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `session_key` int(11) unsigned NOT NULL,
  `create_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `expire_date` datetime NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `session_id_UNIQUE` (`session_key`),
  KEY `FK_Sessions_Users_idx` (`user_id`),
  CONSTRAINT `FK_Sessions_Users` FOREIGN KEY (`user_id`) REFERENCES `Users` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Sessions`
--

LOCK TABLES `Sessions` WRITE;
/*!40000 ALTER TABLE `Sessions` DISABLE KEYS */;
INSERT INTO `Sessions` VALUES (102,608566095,'2016-12-15 18:32:55','2016-12-16 18:32:55',3),(103,459792039,'2016-12-15 18:33:27','2016-12-16 18:33:27',2);
/*!40000 ALTER TABLE `Sessions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Tags`
--

DROP TABLE IF EXISTS `Tags`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Tags` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `text` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `text_UNIQUE` (`text`),
  UNIQUE KEY `id_UNIQUE` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Tags`
--

LOCK TABLES `Tags` WRITE;
/*!40000 ALTER TABLE `Tags` DISABLE KEYS */;
INSERT INTO `Tags` VALUES (14,''),(18,'ay'),(20,'bear'),(17,'bye'),(12,'cool'),(24,'face'),(2,'gary'),(25,'haha'),(1,'happy'),(16,'hi'),(15,'idk'),(23,'lenny'),(21,'mm'),(13,'shrug'),(22,'tasty'),(19,'yay');
/*!40000 ALTER TABLE `Tags` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `hashed_password` varchar(64) NOT NULL,
  `email` varchar(255) NOT NULL,
  `salt` int(11) unsigned NOT NULL,
  `display_kaomoji_id` int(11) unsigned DEFAULT NULL,
  `type` int(11) unsigned NOT NULL DEFAULT '0',
  `register_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `session_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `FK_Users_Kaomojis_idx` (`display_kaomoji_id`),
  KEY `FK_Users_Sessions_idx` (`session_id`),
  CONSTRAINT `FK_Users_Kaomojis` FOREIGN KEY (`display_kaomoji_id`) REFERENCES `Kaomojis` (`id`) ON DELETE NO ACTION ON UPDATE CASCADE,
  CONSTRAINT `FK_Users_Sessions` FOREIGN KEY (`session_id`) REFERENCES `Sessions` (`id`) ON DELETE SET NULL ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `Users`
--

LOCK TABLES `Users` WRITE;
/*!40000 ALTER TABLE `Users` DISABLE KEYS */;
INSERT INTO `Users` VALUES (2,'user','3700adf1f25fab8202c1343c4b0b4e3fec706d57cad574086467b8b3ddf273ec','stevenlam@slammingdesigns.com',12345,2,1,'2016-12-13 05:56:43',103),(3,'mod','2c3eebd2096d8222276a278e47a1ad7e7fa9869298c844bff91d60671f54e3f9','lamsteven@slammingdesigns.com',12345,2,2,'2016-12-13 05:56:43',102);
/*!40000 ALTER TABLE `Users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-12-19 23:35:45
