// In webpack.config.js
const webpack = require('webpack');

module.exports = 
{
	entry: [
	'./app/index.js'
	],
	
	output: 
	{
		path: __dirname + '/dist/static/js',
		filename: "index_bundle.js"
	},
	
	debug: true,
	devtool: 'source-map',
	
	module: 
	{
		loaders: [
		{
			test: /\.js$/, include: __dirname + '/app', 
			loader: "babel-loader",
			query: 
			{
				cacheDirectory: 'babel_cache',
				presets: ['react', 'es2015']
			}	
		}]
	},
	
	watch: true,
	watchOptions: 
	{
	  aggregateTimeout: 300,
	  poll: 3000,
	  ignored: /node_modules/
	},
	
	plugins: [
		new webpack.DefinePlugin(
		{	
			'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV)
		}),
		new webpack.optimize.DedupePlugin()
	],
	
	resolveLoader: 
	{
		root: __dirname + '/node_modules'
	}
};