// src/routes.js
import React from 'react'
import { Route, IndexRoute } from 'react-router'

import Layout from './components/Layout';
import Kaomoji from './components/Kaomoji';
import Create from './components/Create';
import Queue from './components/Queue';
import Search from './components/Search';
import Homepage from './components/Homepage';
import NotFoundPage from './components/NotFoundPage';


const routes = (
  <Route path="/" component={Layout}>
	<IndexRoute component={Homepage}/>
	<Route path="/kaomoji/:kaomojiId" component={Kaomoji}/>
	<Route path="/create/:kaomojiId" component={Create}/>
	<Route path="/create" component={Create}/>
	<Route path="/queue" component={Queue}/>
	<Route path="/search" component={Search}/>
	<Route path="*" component={NotFoundPage}/>
  </Route>
);

export default routes;