import React from 'react';
import { createStore, applyMiddleware, combineReducers } from 'redux'
const RRedux = require('react-redux');
import ReactDOM from 'react-dom';
import thunkMiddleware from 'redux-thunk';

import AppRouter from './components/AppRouter';
import KaomojiReducer from './redux/reducers/KaomojiReducer';
import HomepageReducer from './redux/reducers/HomepageReducer';
import CreateReducer from './redux/reducers/CreateReducer';
import QueueReducer from './redux/reducers/QueueReducer';
import SearchReducer from './redux/reducers/SearchReducer';

const reducers = combineReducers(
{
	Kaomoji: KaomojiReducer,
	Homepage: HomepageReducer,
	Create: CreateReducer,
	Queue: QueueReducer,
	Search: SearchReducer
})

window.store = createStore(
	reducers, 
	applyMiddleware(thunkMiddleware)
);


window.onload = () => 
{
  ReactDOM.render(
	<RRedux.Provider store={store}>
		<AppRouter/>
	</RRedux.Provider>,

	document.getElementById('app'));
};
