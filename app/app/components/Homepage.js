import React from 'react';
import { Link } from 'react-router';
const RRedux = require('react-redux');
import { convertUnicode } from './convertUnicode';
import MiniDisplayKaomoji from './KaomojiDisplays';
import {login, getPopular} from '../redux/reducers/HomepageReducer';
import { updateQuery, searchByTags, performSearch } from '../redux/reducers/SearchReducer';
import { browserHistory } from 'react-router';

var Homepage = props =>
	<div className="mainContainer">
		<Searchbar/>
		<HomepageMain/>
	</div>;
	
var Searchbar = props =>
	<div id="searchBar" className="flexContainer flexRowRev clickable">
		<input type="button" 
				id="searchButton" 
				className="flexBasis button" 
				onClick={e => browserHistory.push("/search")}
				value={convertUnicode("\uf002")}/>
		<input type="edit" id="searchBox" 
				value={props.query}
				placeholder="Search for something..."
				onChange={e => props.updateQuery(e.target.value)}/>	
	</div>;
Searchbar = RRedux.connect(
	
	s => 
	({
		query: s.Search.query
	}),
	
	d =>
	({
		updateQuery: function(input) {d(updateQuery(input))}
	})
	
)(Searchbar);
	
var HomepageMain = props =>
	<div className="flexContainer flexColumn">
		<TopSection/>
		<BigHorizontal/>
		<BottomSection/>		
	</div>;
	
	
var TopSection = props =>
	<div className="flexContainer flexRow">
		<HomepageDescription/>
		<HomepageCreateButton/>
		<HomepageLogin/>
	</div>
	
var HomepageDescription = props =>
	<div id="homepageDescription" className="flexGrow">
		Search for some kaomojis using the search bar at the top <br/>
		Browse through some interesting ones at the bottom, <br/>
		Or, you can try making you own using the button at the right <br/>
		(note that you need to have an account to save or submit any kaomojis).
	</div>;
	
var HomepageCreateButton = props =>
	<div id="homepageCreate" className="flexContainer flexColumn flexBasis">
		<a id="homepageCreateButton clickable">
			<div id="homepageHappyGary">
				{convertUnicode("\u1555\u0028\u0020\u141b\u0020\u0029\u1557\u0020")}
			</div>
			<input type="button"
					id="homepageCreateButtonButton"
					className="flexBasis"
					value="CREATE >>"
					onClick={e => browserHistory.push('create')}/>
		</a>
				
	</div>;
				
var HomepageLogin = React.createClass(
{
	getInitialState () 
	{
		return {
			username: '',
			password: ''
		};
	},
	
	userChanged: function(e)
	{
		this.setState({username: e.target.value});
	},
	
	passChanged: function(e)
	{
		this.setState({password: e.target.value});
	},
	
	handleSubmit: function()
	{
		this.props.login(this.state.username, this.state.password);
	},	
	
	render() 
	{
		if(typeof this.props.user != 'undefined')		
			return (<div/>);
		
		return (
			<div id="homepageLogin" className="flexContainer flexColumn flexBasis">
				<input type="edit" 
						placeholder={convertUnicode("\uf007")}
						onChange={e => this.userChanged(e)}/>
				<input type="edit" 
						placeholder={convertUnicode("\uf084")}
						onChange={e => this.passChanged(e)}/>
				<div id="homepageLoginButtons" className="flexContainer flexRow flexJustSpace">
					<a className="clickable">Sign Up</a>
					<a className="clickable" onClick={this.handleSubmit}>Login</a>
				</div>				
			</div>
		);
	}
});
HomepageLogin = RRedux.connect(
	
	s => 
	({
		user: s.Homepage.user.id
	}),
	
	d =>
	({
		login: function(user, pass) {d(login(user, pass))}
	})
	
)(HomepageLogin);
				


var BigHorizontal = props =>
	<div className="bigHorizontal"/>;
	
var BigVertical = props =>
	<div className="bigVertical"/>;
	



var BottomSection = props =>
	<div className="flexContainer flexRow">
		<TagsSection/>
		<BigVertical/>
		<PopularSection/>
		<BigVertical/>
		<RecentSection/>
	</div>;

var TagsSection = props =>
	<div className="flexGrow">
		<div className="sectionHead">
			Tags
		</div>
		<div id="tagsList" className="text-center">
			<a className="clickable link" onClick={e => props.performSearch("Happy")}>Happy</a>
			<br/>
			<a className="clickable link" onClick={e => props.performSearch("Gary")}>Gary</a>
		</div>
	</div>;
TagsSection = RRedux.connect(
	
	s => 
	({
		
	}),
	
	d =>
	({
		performSearch: function(query) {d(performSearch(query))}
	})
	
)(TagsSection);
	
var PopularSection = React.createClass(
{
	componentDidMount: function ()
	{
		this.props.getPopular();
	},
	
	render: function()
	{
		return(
	
			<div className="flexGrow">
				<div className="sectionHead">
					Popular
				</div>
				<div>
				{
					this.props.popular.map((k, i) =>
					{
						if(i > 3) return;
						return(
							<MiniDisplayKaomoji 
								text={decodeURIComponent(k.text)}
								opacity={1-i*0.25}
								onClick={e => browserHistory.push("/kaomoji/" + k.id)}/>
								)
					})
				}
				</div>
			</div>
		);
	}
});
PopularSection = RRedux.connect(
	
	s => 
	({
		popular: s.Homepage.popular
	}),
	
	d =>
	({
		getPopular: function() {d(getPopular())}
	})
	
)(PopularSection);
				
	
var RecentSection = props =>
	<div className="flexGrow">
		<div className="sectionHead">
			Recent
		</div>
		<div>

		</div>
	</div>;
	
	



















export default Homepage;
	