import React from 'react';
import { Link } from 'react-router';
const RRedux = require('react-redux');
import { convertUnicode } from './convertUnicode';
import {fetchKaomoji, approveKaomoji} from '../redux/reducers/KaomojiReducer'
import { browserHistory } from 'react-router';
import { performSearch } from '../redux/reducers/SearchReducer'
var Kaomoji = React.createClass(
{
	componentDidMount: function()
	{
		this.props.fetch(this.props.params.kaomojiId);
	},
	
	render: function()
	{
		if(this.props.error)
			return (
				<div>
					Kaomoji not found <br/>
					<a className="clickable link" onClick={e => browserHistory.goBack()}>Go Back</a>
				</div>
			);
		
		return (
			<div>
				<div className="mainContainer flexContainer flexColumn">
					<KaomojiDisplay/>
					<KaomojiSub/>
				</div>
			</div>
		);
	}
});
Kaomoji = RRedux.connect(
	
	s => 
	({
		kaomoji: s.Kaomoji.kaomoji,
		error: s.Kaomoji.error
	}),
	
	d =>
	({
		fetch: function(id) {	d(fetchKaomoji(id))	}
	})
	
)(Kaomoji);

function copyToClipboard(text) 
{
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val(text).select();
    document.execCommand("copy");
    $temp.remove();
}


var KaomojiDisplay = props =>
<div className="displayWithButtons">
	<div className="kaomojiDisplay flexBasis">
		{convertUnicode(props.kaomoji.text)}
	</div>
	<KaomojiButtons/>
</div>;
KaomojiDisplay = RRedux.connect(
	s => 
	({
		kaomoji: s.Kaomoji.kaomoji
	})
)(KaomojiDisplay);

var KaomojiButtons = props =>
	<div className="kaomojiButtons flexContainer flexRow flexCenter">
		<a className="clickable button" onClick={e => copyToClipboard(props.kaomoji.text)}>Copy</a>
		<a className="clickable button" onClick={e => browserHistory.push("/create/" + props.kaomoji.id)}>Create With</a>
		{props.mod && !(+props.kaomoji.approved)? 
			<a className="clickable button darkgreen" onClick={e => props.approve(props.kaomoji.id)}>Approve</a>
		:
			""
		}
	</div>;
KaomojiButtons = RRedux.connect(
	
	s => 
	({
		mod: s.Homepage.user.type === 2,
		kaomoji: s.Kaomoji.kaomoji
	}),
	
	d =>
	({
		approve: function(id) {d(approveKaomoji(id))}
	})
	
)(KaomojiButtons);

var KaomojiSub = props =>
	<div className="flexGrow flexContainer flexRowRev">
		<KaomojiStats/>
		<KaomojiDescription/>
	</div>;
	
var KaomojiDescription = props =>
	<div id="kaomojiDescription" className="flexGrow flexContainer flexColumn">
		<div className="title">
			{props.kaomoji.name}
			{(+props.kaomoji.approved)? "" : <span className="red">(Not Approved)</span>}
		</div>
		<div className="description">{props.kaomoji.description}</div>		
	</div>;
KaomojiDescription = RRedux.connect(
	
	s => 
	({
		kaomoji: s.Kaomoji.kaomoji
	})
)(KaomojiDescription);
	
var KaomojiStats = props =>
	<div id="kaomojiStats" className="flexBasis">
		<div><i className="fa fa-heart"></i> {props.kaomoji.likes - props.kaomoji.dislikes}</div>
		<div><i className="fa fa-eye"></i> {props.kaomoji.views}</div>
		<i className="fa fa-hashtag"></i>&nbsp;
			{
				props.tags.map((t, index) => 
					<div key={index}>
						<a className="clickable link"
							onClick={e => props.performSearch(t.text)}>
							#{t.text}</a>
							<span>&nbsp;</span>
					</div>)
			}
	</div>;
KaomojiStats = RRedux.connect(

	s => 
	({
		kaomoji: s.Kaomoji.kaomoji,
		tags: s.Kaomoji.tags
	}),
	
	d =>
	({
		performSearch: function(query) {d(performSearch(query))}
	})
	
)(KaomojiStats);
	

export default Kaomoji;
