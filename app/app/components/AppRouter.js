// src/components/AppRoutes.js
import React from 'react';
import { Router, browserHistory } from 'react-router';
import routes from '../routes';

export default class AppRouter extends React.Component 
{
  render() 
  {
    console.log(routes);
	return (
		<Router history={browserHistory} routes={routes} onUpdate={() => window.scrollTo(0, 0)}/>
    );
  }
}