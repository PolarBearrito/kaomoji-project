import React from 'react';
import { Link } from 'react-router';
const RRedux = require('react-redux');
import { convertUnicode, toUnicode} from './convertUnicode';
import {updateInput, selectTab, appendInput, updateName, updateDescription, updateTags, submitKaomoji, createWithKaomoji } from '../redux/reducers/CreateReducer'
import { browserHistory } from 'react-router';

var Create = React.createClass(
{
	componentDidMount: function()
	{
		//this.props.fetch(this.props.params.kaomojiId);
		if(this.props.params.kaomojiId)
		{
			this.props.createWithKaomoji(this.props.params.kaomojiId);
		}
	},
	
	render: function()
	{
		return(
			<div className="mainContainer">
				<KaomojiInfo/>
				<KaomojiInput/>
				<CreateTabsButtons/>
				<CreateTabs/>
			</div>
		)
	}
});
Create = RRedux.connect(
	
	s => 
	({
	}),
	
	d =>
	({
		createWithKaomoji: function(id) {d(createWithKaomoji(id))}
	})
	
)(Create);

var KaomojiInfo = props =>
	<div id="KaomojiInfo">
		<input type="edit"
				placeholder="Name"
				value={props.name}
				onChange={e => props.updateName(e.target.value)}/>
		<input type="edit"
				placeholder="Description"
				value={props.description}
				onChange={e => props.updateDescription(e.target.value)}/>
		<input type="edit"
				placeholder="Tags"
				value={props.tags}
				onChange={e => props.updateTags(e.target.value)}/>
	</div>;
KaomojiInfo = RRedux.connect(
	
	s => 
	({
		name: s.Create.name,
		description: s.Create.description,
		tags: s.Create.tags,
	}),
	
	d =>
	({
		updateName: function(text) {	d(updateName(text))	},
		updateDescription: function(text) {	d(updateDescription(text))	},
		updateTags: function(text) {	d(updateTags(text))	}
	})
	
)(KaomojiInfo);
	

var KaomojiInput = props =>
	<div id="KaomojiInput" className="displayWithButtons">
		<input type="edit"
				value={props.input}
				onChange={e => props.updateInput(e.target.value)}
				className="kaomojiDisplay"/>
		<CreateButtons/>
	</div>;
KaomojiInput = RRedux.connect(
	
	s => 
	({
		input: s.Create.input,
	}),
	
	d =>
	({
		updateInput: function(text) {	d(updateInput(text))	}
	})
	
)(KaomojiInput);

var CreateButtons = props =>
	<div className="kaomojiButtons flexContainer flexRow flexCenter">
		<a className="clickable button green" onClick={e => props.submit()}>Submit</a>
	</div>;
CreateButtons = RRedux.connect(
	
	s => 
	({
		input: s.Create.input,
		name: s.Create.name,
		description: s.Create.description,
		tags: s.Create.tags
	}),
	
	d =>
	({
		submitKaomoji: function(input, name, description, tags) 
			{	d(submitKaomoji(input, name, description, tags))	}
	}),
	
	(s, d) =>
	({
		submit: () => d.submitKaomoji(s.input, s.name, s.description, s.tags)
	})
	
)(CreateButtons);

var CreateTabsButtons = props =>
	<div className="flexContainer flexRow createTabsButtons">
		<a className={props.selectedTab === "Face" ? props.selected : props.unselected}
			onClick={e => props.selectTab("Face")}>
			Face
		</a>
		<a className={props.selectedTab === "Eyes"? props.selected : props.unselected}
			onClick={e => props.selectTab("Eyes")}>
			Eyes
		</a>
		<a className={props.selectedTab === "Mouth"? props.selected : props.unselected}
			onClick={e => props.selectTab("Mouth")}>
			Mouth
		</a>
		<a className={props.selectedTab === "Hands"? props.selected : props.unselected}
			onClick={e => props.selectTab("Hands")}>
			Hands
		</a>
	</div>;
CreateTabsButtons = RRedux.connect(
	
	s => 
	({
		selectedTab: s.Create.selectedTab,
		selected: "flexBasis clickable button selected",
		unselected: "flexBasis clickable button",
	}),
	
	d =>
	({
		selectTab: function(text) {	d(selectTab(text))	}
	})
	
)(CreateTabsButtons);
	
var CreateTabs = props =>
	<div>
		{props.selectedTab === "Face"? <FaceTab/> : ""}
		{props.selectedTab === "Eyes"? <EyesTab/> : ""}
		{props.selectedTab === "Mouth"? <MouthTab/> : ""}
		{props.selectedTab === "Hands"? <HandsTab/> : ""}
	</div>
CreateTabs = RRedux.connect(
	
	s => 
	({
		selectedTab: s.Create.selectedTab
	})
	
)(CreateTabs);	

var Tab = props =>
<div className="flexContainer flexRow flexWrap piecesTab">
{
	props.pieces.map((p, index) => 
		<Piece key={index}
				text={convertUnicode(p)} 
				onclick={props.appendInput.bind(null, convertUnicode(p))}/>
		)
}
</div>
var FaceTab = RRedux.connect(
	
	s => 
	({
		pieces: 
		[
			"\u0028",
			"\u0029",
			"\u005b",
			"\u005d",
			"\u007b",
			"\u007d",
			"\u0295",
			"\u0294",
			"\u1d98",
			"\u1d85"
		]
	}),
	
	d =>
	({
		appendInput: function(input) {d(appendInput(input))}
	})
	
)(Tab);

var EyesTab = RRedux.connect(
	
	s => 
	({
		pieces: 
		[
			"\u2022",
			"\uff40",
			"\u00b4",
			"\u2267",
			"\u2266",
			"\u0298",
			"\u00b0",
			"\u09ad",
			"\u25d5",
			"\u25d4",
			"\u25e1",
			"\u25e0",
			"\u00f2",
			"\u00f3",
			"\u0ca5",
			"\u25c9"
		]
	}),
	
	d =>
	({
		appendInput: function(input) {d(appendInput(input))}
	})
	
)(Tab);

var MouthTab = RRedux.connect(
	
	s => 
	({
		pieces: 
		[
			"\u1d25",
			"\u2092",
			"\u25cf",
			"\u3268",
			"\u30a8",
			"\u005f",
			"\u03c9",
			"\u2207",
			"\u25e1",
			"\u0414",
			"\u11ba",
			"\ufecc",
			"\ua788",
			"\u0f1d",
			"\u22b1",
			"\u25a1",
			"\uff9b",
			"\uff9f",
			"\u76ca",
			"\u03b5",
			"\u76bf",
			"\u06a1"
		]
	}),
	
	d =>
	({
		appendInput: function(input) {d(appendInput(input))}
	})
	
)(Tab);

var HandsTab = RRedux.connect(
	
	s => 
	({
		pieces: 
		[
			"\u261e",
			"\u0e07",
			"\u1566",
			"\u1564",
			"\u10da",
			"\u256f",
			"\u30ce",
			"\u005f",
			"\u002f",
			"\u00af",
			"\u005c",
			"\u2510",
			"\u250c",
			"\u3064"
		]
	}),
	
	d =>
	({
		appendInput: function(input) {d(appendInput(input))}
	})
	
)(Tab);
	
	
var Piece = props =>
	<a className="clickable button flexBasis"
		onClick={e => props.onclick()}>
			{props.text}
	</a>;


export default Create;
