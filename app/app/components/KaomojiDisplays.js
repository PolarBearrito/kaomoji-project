import React from 'react';
import { Link } from 'react-router';
const RRedux = require('react-redux');
import convertUnicode from './convertUnicode';


const MiniDisplayKaomoji = props =>
	<div className="miniDisplayKaomoji clickable">
		<div style={{opacity: props.opacity || 1}} onClick={props.onClick}>
			{props.text}
		</div>
	</div>;
	

export {MiniDisplayKaomoji as default}