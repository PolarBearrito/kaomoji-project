import React from 'react';
import { Link } from 'react-router';
const RRedux = require('react-redux');
import { convertUnicode, toUnicode} from './convertUnicode';
import { updateQuery, searchByTags, performSearch } from '../redux/reducers/SearchReducer'
import { browserHistory } from 'react-router';


var Search = React.createClass(
{
	componentDidMount: function()
	{
		//this.props.fetch(this.props.params.kaomojiId);
			this.props.searchByTags();
	},
	
	render: function()
	{
		return(
			<div className="mainContainer">
				<SearchHeader/>
				<ResultsList/>
			</div>
		);
	}
});
Search = RRedux.connect(
	
	s => 
	({
		
	}),
	
	d =>
	({
		searchByTags: function() {d(searchByTags())}
	})
	
)(Search);


var SearchHeader = props =>
	<div id="searchBar" className="flexContainer flexRowRev">
		<input type="button" 
				id="searchButton" 
				className="flexBasis button" 
				onClick={e => props.searchByTags()}
				value={convertUnicode("\uf002")}/>
		<input type="edit" id="searchBox" 
				value={props.query}
				placeholder="Search for something..."
				onChange={e => props.updateQuery(e.target.value)}/>	
	</div>;
SearchHeader = RRedux.connect(
	
	s => 
	({
		query: s.Search.query
	}),
	
	d =>
	({
		updateQuery: function(input) {d(updateQuery(input))},
		searchByTags: function() {d(searchByTags())}
	})
	
)(SearchHeader);


var ResultsList = props =>
	<div>
	{
		props.results.map(k =>
			<ResultsItem text={decodeURIComponent(k.text)} onclick={browserHistory.push.bind(null, "/kaomoji/" + k.id)}/>
			)
	}
	</div>
ResultsList = RRedux.connect(
	
	s => 
	({
		results: s.Search.results
	})
	
)(ResultsList);

var ResultsItem = props =>
	<div className="clickable largeKaomoji"
			onClick={e => props.onclick()}>
				{props.text}
	</div>;
	
	



export default Search;
