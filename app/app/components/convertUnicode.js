export function toUnicode(theString = "") 
{
  var unicodeString = '';
  for (var i=0; i < theString.length; i++) {
    var theUnicode = theString.charCodeAt(i).toString(16).toUpperCase();
    while (theUnicode.length < 4) {
      theUnicode = '0' + theUnicode;
    }
    theUnicode = '\\u' + theUnicode;
    unicodeString += theUnicode;
  }
  return unicodeString;
}

export function convertUnicode(input = "") 
{
  return input.replace(/\\u(\w\w\w\w)/g,function(a,b) 
  {
    var charcode = parseInt(b,16);
    return String.fromCharCode(charcode);
  });
}

// THESE WERE TAKEN FROM STACKOVERFLOW, CONTACT THE TEAM FOR EXACT SOURCES