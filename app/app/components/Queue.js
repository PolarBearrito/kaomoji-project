import React from 'react';
import { Link } from 'react-router';
const RRedux = require('react-redux');
import { convertUnicode, toUnicode} from './convertUnicode';
import {fetchQueue} from '../redux/reducers/QueueReducer'
import { browserHistory } from 'react-router';

var Queue = React.createClass(
{
	componentDidMount: function()
	{
		//this.props.fetch(this.props.params.kaomojiId);
			this.props.fetchQueue();
	},
	
	render: function()
	{
		return(
			<div class="mainContainer">
				<QueueHeader/>
				<QueueList/>
			</div>
		);
	}
});
Queue = RRedux.connect(
	
	s => 
	({
		user: s.Homepage.user
	}),
	
	d =>
	({
		fetchQueue: function() {d(fetchQueue())}
	})
	
)(Queue);


var QueueHeader = props =>
	<div className="largeHeader">
		Unappoved Kaomoji
	</div>;

var QueueList = props =>
	<div>
	{
		props.queue.map(k =>
			<QueueItem text={decodeURIComponent(k.text)} onclick={browserHistory.push.bind(null, "/kaomoji/" + k.id)}/>
			)
	}
	</div>
QueueList = RRedux.connect(
	
	s => 
	({
		queue: s.Queue.queue
	})
	
)(QueueList);

var QueueItem = props =>
	<div className="clickable largeKaomoji"
			onClick={e => props.onclick()}>
				{props.text}
	</div>;
	
	



export default Queue;
