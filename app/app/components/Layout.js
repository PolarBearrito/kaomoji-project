import React from 'react';
const RRedux = require('react-redux');
import {logout, getUser} from '../redux/reducers/HomepageReducer';
import { browserHistory } from 'react-router';

var Header = props =>
	<div id="header" >
		<div className="mainContainer flexBasis flexRow flexContainer flexJustSpace">
			<div><a className="clickable" onClick={e => browserHistory.push("/")}>HOME</a></div>
			
			{typeof props.user.id != 'undefined' && +props.user.type === 2?
				<div><a className="clickable" onClick={e => browserHistory.push("/queue")}>QUEUE</a></div> : ""
			}
			{
				typeof props.user.id != 'undefined'? 
					<div>
						Welcome, {props.user.username} &nbsp;
						<a className="clickable" onClick={props.logout}>LOGOUT</a>
					</div> 
				:
				
					props.path === "/"?
						<div/>
					: 
						<div><a className="clickable" onClick={e => browserHistory.push("/")}>LOGIN</a></div>
			}
		</div>
	</div>;
Header = RRedux.connect(
	
	s => 
	({
		user: s.Homepage.user
	}),
	
	d =>
	({
		logout: function() {d(logout())}
	})
	
)(Header);
				

var Layout = React.createClass(
{
	componentDidMount: function()
	{
		this.props.getUser();
	},
	
	render: function()
	{
		return (
		<div>
			<Header path={this.props.route.path}/>
			{this.props.children}
		</div>);
	}
});
Layout = RRedux.connect(
	
	s => 
	({
		
	}),
	
	d =>
	({
		getUser: function() {d(getUser())}
	})
	
)(Layout);



	
export default Layout;