import React from 'react';
import Redux from 'redux';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router'
import { convertUnicode, toUnicode} from '../../components/convertUnicode';
import {fetchKaomoji} from './KaomojiReducer'

export const UPDATE_INPUT = 'UPDATE_INPUT'
export function updateInput(input) 
{
  return {
    type: UPDATE_INPUT,
    input
  }
}

export const SELECT_TAB = 'SELECT_TAB'
export function selectTab(tab) 
{
  return {
    type: SELECT_TAB,
	tab
  }
}

export const APPEND_INPUT = 'APPEND_INPUT'
export function appendInput(input) 
{
  return {
    type: APPEND_INPUT,
	input
  }
}

export const UPDATE_NAME = 'UPDATE_NAME'
export function updateName(input) 
{
  return {
    type: UPDATE_NAME,
	input
  }
}

export const UPDATE_DESCRIPTION = 'UPDATE_DESCRIPTION'
export function updateDescription(input) 
{
  return {
    type: UPDATE_DESCRIPTION,
	input
  }
}

export const UPDATE_TAGS = 'UPDATE_TAGS'
export function updateTags(input) 
{
  return {
    type: UPDATE_TAGS,
	input
  }
}

export function submitKaomoji(text, name, description, tags)
{
  return function (dispatch) 
  {  
	var key = cookie.load('session_key') || 0;
	
	return $.ajax(
	{
		url: `http://127.0.0.1:55581/submitkaomoji`, 
		crossDomain: true,
		data: JSON.stringify(
		{
			text: encodeURIComponent(text),
			name: name,
			description: description,
			key: +key
		}),
		contentType:"application/json",
		type: "POST",
		dataType: "json",
		success: function(data)
		{
			console.log(data);
			tags = tags.trim();
			console.log(tags);
			for(let t of tags.split(/[ ,]+/))
			{
				$.ajax(
				{
					url: `http://127.0.0.1:55581/submittag`, 
					crossDomain: true,
					data: JSON.stringify(
					{
						text: t,
						id: +data.last,
						key: +key
					}),
					contentType:"application/json",
					type: "POST",
					dataType: "json",
					success: function(data2)
					{
						browserHistory.push("/kaomoji/" + data.last);
					}
				});
			}		
		},
		error: function()
		{
			
		}
	});
  }
}

export function createWithKaomoji(id)
{
  return function (dispatch, getState) 
  {  
	return dispatch(fetchKaomoji(id)).then(() =>
	{
		dispatch(updateInput(getState().Kaomoji.kaomoji.text));
		dispatch(updateName(getState().Kaomoji.kaomoji.name));
		dispatch(updateDescription(getState().Kaomoji.kaomoji.description));

	}).then(()=>
	{
		dispatch(updateTags(getState().Kaomoji.tags.reduce((p, c) => {return p + " " + c.text}, "")));
	});
  }
}


// The Reducer Function
const CreateReducer = function(state, action) 
{
  if (state === undefined) 
  {
    state = 
	{
		input: "",
		selectedTab: "",
		
		name: "",
		description: "",
		tags: ""
	};
  }
  
  switch(action.type) 
  {
	  case 'UPDATE_INPUT':
		return Object.assign({}, state, { input: action.input });
	  case 'APPEND_INPUT':
		return Object.assign({}, state, { input: state.input + action.input });
	  case 'SELECT_TAB':
		return Object.assign({}, state, { selectedTab: action.tab });
		
	  case 'UPDATE_NAME':
		return Object.assign({}, state, { name: action.input });
	  case 'UPDATE_DESCRIPTION':
		return Object.assign({}, state, { description: action.input });
	  case 'UPDATE_TAGS':
		return Object.assign({}, state, { tags: action.input });
  }
  return state;
}

export default CreateReducer