import React from 'react';
import Redux from 'redux';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router'

export const LOGIN = 'LOGIN'
function loginUser(user) 
{
  return {
    type: LOGIN,
    user
  }
}

export const LOGOUT = 'LOGOUT'
function logoutUser() 
{
  return {
    type: LOGOUT
  }
}

export const UPDATE_POPULAR = 'UPDATE_POPULAR'
function updatePopular(data) 
{
  return {
    type: UPDATE_POPULAR,
	data
  }
}


export function logout() 
{
  return function (dispatch) 
  {  
	cookie.remove('session_key', { path: '/' });
	dispatch(logoutUser());
	browserHistory.push("/");
  }
}

export function getUser() 
{
  return function (dispatch) 
  {  
	var key = cookie.load('session_key') || 0;
	
	if(key !== 0)
	{
		return $.ajax(
		{
			url: `http://127.0.0.1:55581/userbysession`, 
			crossDomain: true,
			data: JSON.stringify({key:+key}),
			contentType:"application/json",
			type: "POST",
			dataType: "json",
			success: function(data)
			{
				dispatch(loginUser(data));
			},
			error: function() {console.log("hi");}
		});
	}
		return;
  }

}

export function getPopular() 
{
  return function (dispatch) 
  {  

	return $.ajax(
	{
		url: `http://127.0.0.1:55581/topkaomoji`, 
		crossDomain: true,
		contentType:"application/json",
		type: "GET",
		dataType: "json",
		success: function(data)
		{
			dispatch(updatePopular(data));
		},
		error: function() {console.log("hi");}
	});
  }

}







export function login(u, p) 
{
  return function (dispatch) 
  {  
	return $.ajax(
	{
		url: `http://127.0.0.1:55581/login`, 
		crossDomain: true,
		data: JSON.stringify(
		{
			username: u,
			password: p
		}),
		contentType:"application/json",
		type: "POST",
		dataType: "json",
		success: function(data)
		{
			if(data.key)
			{
				cookie.save('session_key', data.key, { path: '/' });
				$.ajax(
				{
					url: `http://127.0.0.1:55581/userbysession`, 
					crossDomain: true,
					data: JSON.stringify(data),
					contentType:"application/json",
					type: "POST",
					dataType: "json",
					success: function(data)
					{
						dispatch(loginUser(data));
					},
					error: function() {console.log("hi");}
				});
			}
		}
	});
  }
}




// The Reducer Function
const HomepageReducer = function(state, action) 
{
  if (state === undefined) 
  {
    state = 
	{
		user: {},
		popular: []
	};
  }
  
  switch(action.type) 
  {
	  case 'LOGIN':
		console.log(action.user);
		return Object.assign({}, state, { user: action.user });
	  case 'LOGOUT':
		return Object.assign({}, state, { user: {} });
	  case 'UPDATE_POPULAR':
		return Object.assign({}, state, { popular: action.data });
  }
  return state;
}

export default HomepageReducer