import React from 'react';
import Redux from 'redux';
import cookie from 'react-cookie';



export const RECEIVE_KAOMOJI = 'RECEIVE_KAOMOJI'
function receiveKaomoji(kaomoji) 
{
  return {
    type: RECEIVE_KAOMOJI,
    kaomoji
  }
}

export const RECEIVE_KAOMOJI_TAGS = 'RECEIVE_KAOMOJI_TAGS'
function receiveKaomojiTags(tags) 
{
  return {
    type: RECEIVE_KAOMOJI_TAGS,
    tags
  }
}

export const RECEIVE_KAOMOJI_ERROR = 'RECEIVE_KAOMOJI_ERROR'
function receiveKaomojiError() 
{
  return {
    type: RECEIVE_KAOMOJI_ERROR
  }
}

export const UPDATE_KAOMOJI_APPROVED = 'UPDATE_KAOMOJI_APPROVED'
function updateKaomojiApproved() 
{
  return {
    type: UPDATE_KAOMOJI_APPROVED
  }
}


export function fetchKaomoji(id)
{
  return function (dispatch, getState) 
  {  
	var key = cookie.load('session_key') || 0;
	
	return $.ajax(
	{
		url: `http://127.0.0.1:55581/kaomoji`, 
		crossDomain: true,
		data: JSON.stringify(
		{
			id: +id,
			key: +key
		}),
		contentType:"application/json",
		type: "POST",
		dataType: "json",
		success: function(data)
		{
			var data2 = data;
			data2.text = decodeURIComponent(data.text);
			
			dispatch(receiveKaomoji(data2));
			$.ajax(
			{
				url: `http://127.0.0.1:55581/upview`, 
				crossDomain: true,
				data: JSON.stringify(
				{
					id: +id
				}),
				contentType:"application/json",
				type: "POST",
				dataType: "json",
				success: function(data)
				{
					
				}
			});
			
			$.ajax(
			{
				url: `http://127.0.0.1:55581/tagsforkaomoji`, 
				crossDomain: true,
				data: JSON.stringify(
				{
					id: +id,
					key: +key
				}),
				contentType:"application/json",
				type: "POST",
				dataType: "json",
				success: function(data)
				{
					dispatch(receiveKaomojiTags(data));
				}
			});
			
			
		},
		error: function()
		{
			dispatch(receiveKaomojiError());
		}
	});
  }
}

export function approveKaomoji(id)
{
  return function (dispatch, getState) 
  {  
	var key = cookie.load('session_key') || 0;
	
	return $.ajax(
	{
		url: `http://127.0.0.1:55581/approvekaomoji`, 
		crossDomain: true,
		data: JSON.stringify(
		{
			id: +id,
			key: +key
		}),
		contentType:"application/json",
		type: "POST",
		dataType: "json",
		success: function(data)
		{
			dispatch(updateKaomojiApproved());
		},
		error: function()
		{
		}
	});
  }
}


// The Reducer Function
const KaomojiReducer = function(state, action) 
{
  if (state === undefined) 
  {
    state = 
	{
		kaomoji: {},
		tags: [],
		error: false
	};
  }
  
  switch(action.type) 
  {
	  case 'UPDATE_KAOMOJI_APPROVED':
		return Object.assign({}, state, { kaomoji: Object.assign({}, state.kaomoji, {approved: true}) });
      case 'RECEIVE_KAOMOJI_TAGS':
		return Object.assign({}, state, { tags: action.tags });
	  case 'RECEIVE_KAOMOJI_ERROR':
		return Object.assign({}, state, { error: true });
	  case 'RECEIVE_KAOMOJI':
		return Object.assign({}, state, { kaomoji: action.kaomoji, error: false  });
  }
  return state;
}

export default KaomojiReducer