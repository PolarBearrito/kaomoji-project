import React from 'react';
import Redux from 'redux';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router'

export const UPDATE_RESULTS = 'UPDATE_RESULTS'
function updateResults(results) 
{
  return {
    type: UPDATE_RESULTS,
    results
  }
}

export const UPDATE_QUERY = 'UPDATE_QUERY'
export function updateQuery(query) 
{
  return {
    type: UPDATE_QUERY,
    query
  }
}

export function searchByTags() 
{
  return function (dispatch, getState) 
  {  
		var query = getState().Search.query;
		var arrQuery = query.split(/[ ,]+/);
		var queryString = "|" + arrQuery.join("|") + "|";	
	
		return $.ajax(
		{
			url: `http://127.0.0.1:55581/findbytag`, 
			crossDomain: true,
			data: JSON.stringify({tags:queryString}),
			contentType:"application/json",
			type: "POST",
			dataType: "json",
			success: function(data)
			{
				dispatch(updateResults(data));
			},
			error: function() {console.log("hi");}
		});
  }

}

export function performSearch(query) 
{
  return function (dispatch, getState) 
  {  
		dispatch(updateQuery(query));
		
		browserHistory.push("/search");
  }

}



// The Reducer Function
const SearchReducer = function(state, action) 
{
  if (state === undefined) 
  {
    state = 
	{
		query: "",
		results: []
	};
  }
  
  switch(action.type) 
  {
	  case 'UPDATE_RESULTS':
		return Object.assign({}, state, { results: action.results });
		case 'UPDATE_QUERY':
		return Object.assign({}, state, { query: action.query });
  }
  return state;
}

export default SearchReducer