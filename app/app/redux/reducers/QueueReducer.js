import React from 'react';
import Redux from 'redux';
import cookie from 'react-cookie';
import { browserHistory } from 'react-router'

export const UPDATE_QUEUE = 'UPDATE_QUEUE'
function updateQueue(queue) 
{
  return {
    type: UPDATE_QUEUE,
    queue
  }
}

export function fetchQueue() 
{
  return function (dispatch) 
  {  
	var key = cookie.load('session_key') || 0;
	
	if(key !== 0)
	{
		return $.ajax(
		{
			url: `http://127.0.0.1:55581/notapproved`, 
			crossDomain: true,
			data: JSON.stringify({key:+key}),
			contentType:"application/json",
			type: "POST",
			dataType: "json",
			success: function(data)
			{
				dispatch(updateQueue(data));
			},
			error: function() {console.log("hi");}
		});
	}
		return;
  }

}



// The Reducer Function
const QueueReducer = function(state, action) 
{
  if (state === undefined) 
  {
    state = 
	{
		queue: []
	};
  }
  
  switch(action.type) 
  {
	  case 'UPDATE_QUEUE':
		return Object.assign({}, state, { queue: action.queue });
  }
  return state;
}

export default QueueReducer