#!/usr/bin/env bash

sudo apt-get update
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo apt-get install -y build-essential

sudo apt-get install -y git
sudo apt-get install -y cmake
sudo apt-get install -y clang
sudo apt-get install -y libboost-all-dev
sudo apt-get install -y libmicrohttpd-dev

sudo apt-get install -y valgrind

sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
sudo debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
sudo apt-get -y install mysql-server
sudo apt-get -y install mysql-client
sudo apt-get -y install libmysqlclient-dev
sudo apt-get -y install libmysql++-dev
sudo apt-get -y install libssl-dev

sudo service mysql start

sudo sed -i 's/bind-address/#bind-address/g' /etc/mysql/mysql.conf.d/mysqld.cnf
sudo sed -i 's/skip-external-locking/#skip-external-locking/g' /etc/mysql/mysql.conf.d/mysqld.cnf

sudo mysql -u root --password=root -e "create user 'root'@'%' identified by 'root'; grant all privileges on *.* to 'root'@'%' with grant option; flush privileges;"

cd db

sudo mysql -u root --password=root < dump.sql

sudo service mysql restart

cd ../app

sudo npm install

sudo npm install --save-dev webpack -g

sudo npm install --save-dev http-server -g

sudo npm install --save express -g
sudo npm install --save morgan -g

