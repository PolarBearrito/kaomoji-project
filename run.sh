#!/usr/bin/env bash

cd app
sudo NODE_ENV=production node_modules/.bin/babel-node --presets 'react,es2015' server.js

cd ../api
sudo ./api_server