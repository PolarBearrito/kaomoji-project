#include <silicon/api.hh>
#include <silicon/backends/mhd.hh>
#include <silicon/middlewares/mysql_connection.hh>
#include <silicon/middlewares/hashmap_session.hh>
#include "symbols.hh"

#include "boost/random.hpp"
#include "boost/generator_iterator.hpp"

#include <chrono>

#include <iostream>
#include <sstream>
#include <string>
#include <iomanip>

#include <openssl/sha.h>

#include <vector>

using namespace sl; // Silicon namespace
using namespace s; // Symbols namespace

using namespace std;
string sha256(const string str)
{
    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, str.c_str(), str.size());
    SHA256_Final(hash, &sha256);
    stringstream ss;
    for(int i = 0; i < SHA256_DIGEST_LENGTH; i++)
    {
        ss << hex << setw(2) << setfill('0') << (int)hash[i];
    }
    return ss.str();
}


struct session_data
{
  // The constructor must create a empty session.
  session_data() : user_id (0) {}
  // The constructor must empty the session
  // so the middleware can recycle it.
  ~session_data() { logout(); }

  void logout() { user_id = 0; }

  int user_id;
};

int genrand()
{
	typedef boost::mt19937 RNGType;
	RNGType rng(std::chrono::high_resolution_clock::now().time_since_epoch().count());
	boost::uniform_int<> one_to_alot( 1, 1000000000 );
	boost::variate_generator< RNGType, boost::uniform_int<> > dice(rng, one_to_alot);
	return dice();
}


// Define the API:
auto hello_api = http_api(

  // The hello world procedure.
  GET / _hello = [] () { return D(_message = "Hello world."); },
  
	
	OPTIONS / _kaomoji = [] () { return D(_message = "Hello world."); },
	  
	POST / _kaomoji * post_parameters(_id = int(), _key = int()) = [] (auto p, mysql_connection& db)
    {
		auto r = D
		(
			_id 			= int(), 
			_text 			= std::string(),
			_name 			= std::string(),
			_description 	= std::string(),
			_create_date 	= std::string(),
			_modified_date 	= std::string(),
			_views 			= std::string(),
			_likes 			= std::string(),
			_dislikes 		= std::string(),
			_approved 		= std::string(),
			_user_id 		= int()
		);
		
		typedef decltype(r) R;
		R r2;
		db("SELECT * from Kaomojis where id = ?")(p.id) >> r;
		
		if(r.approved.compare("1") == 0)
			return r;
		
		if(p.key == 0)
			return r2;
		
		auto sess = D
		(
			_id					= int(),
			_count				= int(),
			_expired			= bool()
		);
		db("SELECT DISTINCT id, COUNT(id) AS count, expire_date < CURRENT_TIMESTAMP AS expired FROM Sessions WHERE session_key = ? GROUP BY id")(p.key) >> sess;
		
		if(sess.expired)
			return r2;
			
		auto user = D
		(
			_id 				= int(), 
			_username 			= std::string(),
			_email 				= std::string(),
			_display_kaomoji_id = int(),
			_type 				= int(),
			_register_date		= std::string()
		);
		
		if(sess.count == 1)
		{

			db("SELECT U.id, U.username, U.email, U.display_kaomoji_id, U.type, U.register_date FROM Sessions S JOIN Users U ON S.user_id = U.id WHERE session_key = ?")(p.key) >> user;
			
			if(user.type == 0)
			{
				if(r.user_id == user.id)
					return r;
			}
			
			return r;
		}
		
		return r2;
	},
		
		
  
	GET / _kaomoji / _id[int()]	= [] (auto p, mysql_connection& db) 
	{
		auto r = D
		(
			_id 			= int(), 
			_text 			= std::string(),
			_name 			= std::string(),
			_description 	= std::string(),
			_create_date 	= std::string(),
			_modified_date 	= std::string(),
			_views 			= std::string(),
			_likes 			= std::string(),
			_dislikes 		= std::string(),
			_approved 		= std::string(),
			_user_id 		= int()
		);
		db("SELECT * from Kaomojis where id = ?")(p.id) >> r;
		return r;
	},
	
	GET / _user / _id[int()] = [] (auto p, mysql_connection& db) 
	{
		auto r = D
		(
			_id 				= int(), 
			_username 			= std::string(),
			_email 				= std::string(),
			_display_kaomoji_id = int(),
			_type 				= int(),
			_register_date		= std::string()
		);
		db("SELECT id, username, email, display_kaomoji_id, type, register_date from Users where id = ?")(p.id) >> r;
		return r;
	},
	
	
	OPTIONS / _tagsforkaomoji = [] () { return D(_message = "Hello world."); },
	
	POST / _tagsforkaomoji * post_parameters(_id = int(), _key = int()) = [] (auto p, mysql_connection& db)
    {
		auto tag = D
		(
			_id 			= int(), 
			_text 			= std::string()
		);
		
		typedef decltype(tag) Tag;
			
		vector<Tag> x = {};
		
		db("SELECT t.* FROM Kaomojis_Tags AS k JOIN Tags AS t ON k.tag_id = t.id WHERE kaomoji_id = ?")(p.id) | [&x] (Tag tag) mutable 
		{	
			x.push_back(tag);
		};
		
		if(x.empty())
			return x;
		
		auto r = D
		(
			_id 			= int(), 
			_text 			= std::string(),
			_name 			= std::string(),
			_description 	= std::string(),
			_create_date 	= std::string(),
			_modified_date 	= std::string(),
			_views 			= std::string(),
			_likes 			= std::string(),
			_dislikes 		= std::string(),
			_approved 		= std::string(),
			_user_id 		= int()
		);
		
		db("SELECT * from Kaomojis where id = ?")(p.id) >> r;
		
		if(r.approved.compare("1") == 0)
			return x;
		
		if(p.key == 0)
			x.clear();
		
		auto sess = D
		(
			_id					= int(),
			_count				= int(),
			_expired			= bool()
		);
		db("SELECT DISTINCT id, COUNT(id) AS count, expire_date < CURRENT_TIMESTAMP AS expired FROM Sessions WHERE session_key = ? GROUP BY id")(p.key) >> sess;
		
		if(sess.expired)
			x.clear();
			
		auto user = D
		(
			_id 				= int(), 
			_username 			= std::string(),
			_email 				= std::string(),
			_display_kaomoji_id = int(),
			_type 				= int(),
			_register_date		= std::string()
		);
		
		if(sess.count == 1)
		{

			db("SELECT U.id, U.username, U.email, U.display_kaomoji_id, U.type, U.register_date FROM Sessions S JOIN Users U ON S.user_id = U.id WHERE session_key = ?")(p.key) >> user;
			
			if(user.type == 0)
			{
				if(r.user_id == user.id)
					return x;
			}
			
			return x;
		}
		x.clear();
		return x;
	},
	  
	OPTIONS / _login = [] () { return D(_message = "Hello world."); },
	OPTIONS / _userbysession = [] () { return D(_message = "Hello world."); },
	  
	POST / _login * post_parameters(_username = string(), _password = string()) = [] (auto p, mysql_connection& db)
    {
      	auto r = D
		(
			_id					= int(),
			_salt 				= string(),
			_hashed_password 	= string()
		);
		db("SELECT id, salt, hashed_password from Users where username = ?")(p.username) >> r;
		
		string test = p.password.append(r.salt);
		
		bool result = sha256(test).compare(r.hashed_password);
		
		auto s = D
		(
			_last				= int()
		);
	
		
		if(result == 0)
		{
			db("DELETE FROM Sessions WHERE user_id = ?")(r.id);
			
			int key = genrand();
			
			time_t now = time(NULL);
			time_t expire = now + 60*60*24;
			int expires = (int)expire;
			
			db("INSERT INTO Sessions (session_key,  expire_date, user_id) VALUES(?, FROM_UNIXTIME(?), ?)")(key, expires, r.id);
			
			db("SELECT last_insert_id() as last")() >> s;

			
			db("UPDATE Users SET session_id = ? WHERE id = ?")(s.last, r.id);
			
			return D(_key = key);
		}
		
		return D(_key = 0);
    },
	
	POST / _userbysession * post_parameters(_key = int()) = [] (auto p, mysql_connection& db)
    {
      	auto r = D
		(
			_id					= int(),
			_count				= int(),
			_expired			= bool()
		);
		db("SELECT DISTINCT id, COUNT(id) AS count, expire_date < CURRENT_TIMESTAMP AS expired FROM Sessions WHERE session_key = ? GROUP BY id")(p.key) >> r;
		
		auto s = D
		(
			_id 				= int(), 
			_username 			= std::string(),
			_email 				= std::string(),
			_display_kaomoji_id = int(),
			_type 				= int(),
			_register_date		= std::string()
		);
		
		if(r.count == 1 && !r.expired)
		{

			db("SELECT U.id, U.username, U.email, U.display_kaomoji_id, U.type, U.register_date FROM Sessions S JOIN Users U ON S.user_id = U.id WHERE session_key = ?")(p.key) >> s;
			
			return s;
		}
		
		return s;
    },
	
	OPTIONS / _logout = [] () { return D(_message = "Hello world."); },
	  
	POST / _logout * post_parameters(_key = int()) = [] (auto p, mysql_connection& db)
    {
		db("DELETE FROM Sessions WHERE id = ?")(p.key);		
	},
	
	OPTIONS / _submitkaomoji = [] () { return D(_message = "Hello world."); },
	  
	POST / _submitkaomoji * post_parameters(_text = string(), _name = string(), _description = string(), _key = int()) = [] (auto p, mysql_connection& db)
    {
		auto r = D
		(
			_id					= int(),
			_count				= int(),
			_expired			= bool()
		);
		db("SELECT DISTINCT id, COUNT(id) AS count, expire_date < CURRENT_TIMESTAMP AS expired FROM Sessions WHERE session_key = ? GROUP BY id")(p.key) >> r;
		
		auto s = D
		(
			_id 				= int(), 
			_username 			= std::string(),
			_email 				= std::string(),
			_display_kaomoji_id = int(),
			_type 				= int(),
			_register_date		= std::string()
		);
		
		auto new_key = D
		(
			_last				= int()
		);
		
		if(r.count == 1 && !r.expired)
		{

			db("SELECT U.id, U.username, U.email, U.display_kaomoji_id, U.type, U.register_date FROM Sessions S JOIN Users U ON S.user_id = U.id WHERE session_key = ?")(p.key) >> s;
			
			db("INSERT INTO Kaomojis (text, name, description, user_id) VALUES(?, ?, ?, ?)")(p.text, p.name, p.description, s.id);

			db("SELECT last_insert_id() as last")() >> new_key;
			return new_key;
		}
		new_key.last = -1;
		return new_key;
	},
	
	OPTIONS / _submittag = [] () { return D(_message = "Hello world."); },
	  
	POST / _submittag * post_parameters(_text = string(), _id = int(), _key = int()) = [] (auto p, mysql_connection& db)
    {		
		auto r = D
		(
			_id					= int(),
			_count				= int(),
			_expired			= bool()
		);
		db("SELECT DISTINCT id, COUNT(id) AS count, expire_date < CURRENT_TIMESTAMP AS expired FROM Sessions WHERE session_key = ? GROUP BY id")(p.key) >> r;
		
		auto new_key = D
		(
			_last				= int()
		);
		
		auto check = D
		(
			_id					= int(),
			_count				= int()
		);
		
		db("SELECT count(id) AS count, id FROM Tags WHERE text = ? GROUP BY id")(p.text) >> check;
		
		if(r.count == 1 && !r.expired)
		{			
			if(check.count == 0)
			{
				db("INSERT INTO Tags (text) VALUES(?)")(p.text);			
			
				db("SELECT last_insert_id() as last")() >> new_key;
				
				check.id = new_key.last;
			}
						
			db("INSERT INTO Kaomojis_Tags (kaomoji_id, tag_id) VALUES(?, ?)")(p.id, check.id);
		}
		
		return D(_message = "good");
	},
	
	OPTIONS / _upview = [] () { return D(_message = "Hello world."); },
	  
	POST / _upview * post_parameters(_id = int()) = [] (auto p, mysql_connection& db)
    {		
		db("UPDATE Kaomojis SET views = views + 1 WHERE id = ?")(p.id);
				
		return D(_message = "good");
	},
	  
    OPTIONS / _approvekaomoji = [] () { return D(_message = "Hello world."); },
	  
	POST / _approvekaomoji * post_parameters(_id = int(), _key = int()) = [] (auto p, mysql_connection& db)
    {		
		auto r = D
		(
			_id					= int(),
			_count				= int(),
			_expired			= bool()
		);
		
		db("SELECT DISTINCT id, COUNT(id) AS count, expire_date < CURRENT_TIMESTAMP AS expired FROM Sessions WHERE session_key = ? GROUP BY id")(p.key) >> r;
		
		auto s = D
		(
			_id 				= int(), 
			_username 			= std::string(),
			_email 				= std::string(),
			_display_kaomoji_id = int(),
			_type 				= int(),
			_register_date		= std::string()
		);
		
		db("SELECT U.id, U.username, U.email, U.display_kaomoji_id, U.type, U.register_date FROM Sessions S JOIN Users U ON S.user_id = U.id WHERE session_key = ?")(p.key) >> s;			
			
		if(r.count == 1 && !r.expired && s.type == 2)
		{
			db("UPDATE Kaomojis SET approved = b'1' WHERE id = ?")(p.id);
		}
				
		return D(_message = "good");
	},
	
	OPTIONS / _notapproved = [] () { return D(_message = "Hello world."); },
	  
	POST / _notapproved * post_parameters(_key = int()) = [] (auto p, mysql_connection& db)
    {		
		auto r = D
		(
			_id					= int(),
			_count				= int(),
			_expired			= bool()
		);
		
		db("SELECT DISTINCT id, COUNT(id) AS count, expire_date < CURRENT_TIMESTAMP AS expired FROM Sessions WHERE session_key = ? GROUP BY id")(p.key) >> r;
		
		auto s = D
		(
			_id 				= int(), 
			_username 			= std::string(),
			_email 				= std::string(),
			_display_kaomoji_id = int(),
			_type 				= int(),
			_register_date		= std::string()
		);
		
		db("SELECT U.id, U.username, U.email, U.display_kaomoji_id, U.type, U.register_date FROM Sessions S JOIN Users U ON S.user_id = U.id WHERE session_key = ?")(p.key) >> s;	

		auto k = D
		(
			_id 			= int(), 
			_text 			= std::string(),
			_name 			= std::string(),
			_description 	= std::string(),
			_create_date 	= std::string(),
			_modified_date 	= std::string(),
			_views 			= std::string(),
			_likes 			= std::string(),
			_dislikes 		= std::string(),
			_approved 		= std::string(),
			_user_id 		= int()
		);		
		
		typedef decltype(k) K;
			
		vector<K> x = {};
			
		if(r.count == 1 && !r.expired && s.type == 2)
		{
			db("SELECT * FROM Kaomojis WHERE approved = 0")() | [&x] (K k) mutable 
			{	
				x.push_back(k);
			};
			
			if(x.empty())
				return x;	
		}
				
		return x;
	},
	
	OPTIONS / _topkaomoji = [] () { return D(_message = "Hello world."); },
	  
	GET / _topkaomoji = [] (auto p, mysql_connection& db) 
	{
		auto k = D
		(
			_id 			= int(), 
			_text 			= std::string(),
			_name 			= std::string(),
			_description 	= std::string(),
			_create_date 	= std::string(),
			_modified_date 	= std::string(),
			_views 			= std::string(),
			_likes 			= std::string(),
			_dislikes 		= std::string(),
			_approved 		= std::string(),
			_user_id 		= int()
		);		
		
		typedef decltype(k) K;
			
		vector<K> x = {};
			
		db("SELECT * FROM kao.Kaomojis WHERE approved = '1' ORDER BY views DESC")() | [&x] (K k) mutable 
		{	
			x.push_back(k);
		};
			
		return x;
	},
	
	OPTIONS / _findbytag = [] () { return D(_message = "Hello world."); },
	  
	POST / _findbytag * post_parameters(_tags = string()) = [] (auto p, mysql_connection& db) 
	{
		auto k = D
		(
			_id 			= int(), 
			_text 			= std::string(),
			_name 			= std::string(),
			_description 	= std::string(),
			_create_date 	= std::string(),
			_modified_date 	= std::string(),
			_views 			= std::string(),
			_likes 			= std::string(),
			_dislikes 		= std::string(),
			_approved 		= std::string(),
			_user_id 		= int()
		);		
		
		typedef decltype(k) K;
			
		vector<K> x = {};
			
		db("SELECT DISTINCT k.* FROM Tags t JOIN Kaomojis_Tags kt ON kt.tag_id = t.id JOIN Kaomojis k ON kt.kaomoji_id = k.id WHERE ? LIKE concat('%|',t.text,'|%')")(p.tags) | [&x] (K k) mutable 
		{	
			x.push_back(k);
		};
			
		return x;
	}	
);

auto middlewares = std::make_tuple(
   mysql_connection_factory("localhost", "root", "root", "kao"),
   hashmap_session_factory<session_data>()
);

int main()
{
  // Serve hello_api via microhttpd using the json format:
  sl::mhd_json_serve(hello_api, middlewares, 12345);
}