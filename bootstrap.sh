#!/usr/bin/env bash

sudo apt-get update
curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
sudo apt-get install -y nodejs

sudo apt-get install -y build-essential

sudo su

apt-get install -y git
apt-get install -y cmake
apt-get install -y clang
apt-get install -y libboost-all-dev
apt-get install -y libmicrohttpd-dev

apt-get install -y valgrind

debconf-set-selections <<< 'mysql-server mysql-server/root_password password root'
debconf-set-selections <<< 'mysql-server mysql-server/root_password_again password root'
apt-get -y install mysql-server
apt-get -y install mysql-client
apt-get -y install libmysqlclient-dev
apt-get -y install libmysql++-dev
apt-get -y install libssl-dev

service mysql start

sed -i 's/bind-address/#bind-address/g' /etc/mysql/mysql.conf.d/mysqld.cnf
sed -i 's/skip-external-locking/#skip-external-locking/g' /etc/mysql/mysql.conf.d/mysqld.cnf

mysql -u root --password=root -e "create user 'root'@'%' identified by 'root'; grant all privileges on *.* to 'root'@'%' with grant option; flush privileges;"

cd /vagrant/db

mysql -u root --password=root < dump.sql

service mysql restart

cd /vagrant
mkdir api
mkdir app
cd app

npm install

npm install --save-dev webpack -g

npm install --save-dev http-server -g

npm install --save express -g
npm install --save morgan -g

